var utils = {};
(function () {

    function getVerifiedData() {
        var data = {};
        data.amount = document.getElementById("customerAmount");
    }

    function generateScratchCard(amount, currentLastIndex, finalData) {

        // const data = getVerifiedData();
        var lastIndexValue = currentLastIndex;
        var values = Math.floor(amount / 1000);
        //Master list of scratch cards.
        var scratchCards = [];
        getNumberOfScratchCardsTobeGenerated();
        return finalData;


        function getNumberOfScratchCardsTobeGenerated() {

            //Have to create more than one card.
            if (values <= 10 && !isOutOfRange(lastIndexValue, values)) {
                //generate a simple card
                var scratchCard = {
                    startIndex: 0,
                    endIndex: 0,
                    isOpened: false
                };
                scratchCard.startIndex = lastIndexValue + 1;
                scratchCard.endIndex = lastIndexValue + values;
                scratchCards.push(scratchCard);
            } else if (values <= 10) {
                // Out of range, reset range and create a simple card
                var scratchCard = {
                    startIndex: 0,
                    endIndex: 0,
                    isOpened: false
                };
                var remainingSpace = 99 - lastIndexValue;
                if (remainingSpace < 0)
                    return;
                if (lastIndexValue == 99)
                    scratchCard.startIndex = 0;
                else
                    scratchCard.startIndex = 99 - remainingSpace + 1;
                scratchCard.endIndex = values - remainingSpace - 1;
                scratchCards.push(scratchCard);
            } else {
                //values is greater than 10, more than one card needs to be generated
                if (!isOutOfRange(lastIndexValue, values)) {
                    //getRanges
                    while (values > 10) {
                        var scratchCard = {
                            startIndex: 0,
                            endIndex: 0,
                            isOpened: false
                        };
                        values = values - 10;
                        scratchCard.startIndex = lastIndexValue + 1;
                        scratchCard.endIndex = lastIndexValue + 10;
                        lastIndexValue = lastIndexValue + 10;
                        scratchCards.push(scratchCard);
                    }
                    var scratchCard = {
                        startIndex: 0,
                        endIndex: 0,
                        isOpened: false
                    };
                    scratchCard.startIndex = lastIndexValue + 1;
                    scratchCard.endIndex = lastIndexValue + values;
                    scratchCards.push(scratchCard);
                } else {
                    //fix and get ranges.
                    var scratchCard = {
                        startIndex: 0,
                        endIndex: 0,
                        isOpened: false
                    };
                    //Add 10 elements, fill remaining space first and for remaining elements start from 0.
                    var remainingSpace = 99 - lastIndexValue;

                    if (remainingSpace >= 0 && remainingSpace < 10) {
                        if (lastIndexValue == 99)
                            scratchCard.startIndex = 0;
                        else
                            scratchCard.startIndex = 99 - remainingSpace + 1;
                        lastIndexValue = 10 - remainingSpace - 1;
                        scratchCard.endIndex = lastIndexValue;
                        scratchCards.push(scratchCard);
                        values = values - 10;
                    }
                    var tempStartIndex;
                    var tempLastIndex;
                    while (values > 10) {
                        var scratchCard = {
                            startIndex: 0,
                            endIndex: 0,
                            isOpened: false
                        };
                        //Store it in temp variables for easy condition check.
                        tempStartIndex = lastIndexValue + 1;
                        tempLastIndex = lastIndexValue + 10;

                        //Values still out of range, go recusrsive. 
                        if (tempStartIndex > 99 || tempLastIndex > 99) {
                            arguments.callee(amount, currentLastIndex);
                        }
                        //Break execution post recursion stack calls.
                        if (tempStartIndex > 99 || tempLastIndex > 99) {
                            return;
                        }
                        values = values - 10;
                        scratchCard.startIndex = tempStartIndex;
                        scratchCard.endIndex = tempLastIndex;
                        lastIndexValue = lastIndexValue + 10;
                        scratchCards.push(scratchCard);
                    }
                    if (isOutOfRange(lastIndexValue, values)) {
                        //Handle the last element which is out of range
                        if((values > 0 && values < 10) && lastIndexValue < 100){
                            var scratchCard = {
                                startIndex: 0,
                                endIndex: 0,
                                isOpened: false
                            };
                            var remainingSpace = 99 - lastIndexValue;
                            if (remainingSpace < 0)
                                return;
                            if (lastIndexValue == 99)
                                scratchCard.startIndex = 0;
                            else
                                scratchCard.startIndex = 99 - remainingSpace + 1;
                            scratchCard.endIndex = values - remainingSpace - 1;
                            scratchCards.push(scratchCard);
                            finalData = prepareScratchCardObject(scratchCards, amount);
                            return;
                        }
                        if (scratchCards.length > 0)
                            finalData = prepareScratchCardObject(scratchCards, amount);
                        return;
                    }

                    //Finally add remaining elements, will always be less than 10.
                    var scratchCard = {
                        startIndex: 0,
                        endIndex: 0,
                        isOpened: false
                    };
                    scratchCard.startIndex = lastIndexValue + 1;
                    scratchCard.endIndex = lastIndexValue + values;
                    lastIndexValue = lastIndexValue + values;
                    scratchCards.push(scratchCard);

                }
            }

            //Finally return scratch card object which is ready to push to database.
            const tempCards = scratchCards;
            scratchCards = [];
            finalData = prepareScratchCardObject(tempCards, amount);
            return;
        }
    }

    function isOutOfRange(lastIndexValue, values) {
        return (lastIndexValue + values) > 99 ? true : false;
    }

    function prepareScratchCardObject(generatedScratchCard, amount) {
        return {
            amount: amount,
            noOfCards: generatedScratchCard.length,
            values: generatedScratchCard,
            lastIndexValue: generatedScratchCard[generatedScratchCard.length - 1].endIndex
        };
    }

    utils.generateScratchCard = generateScratchCard;
})()