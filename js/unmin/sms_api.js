function sendMessage(invoiceNumber, password, phoneNumber) {
  $.support.cors = true;

// flow juturu scrach cardsconfirmation template in MSG91
  const inputCustomerInvoiceNumber = 'your purchase: '+invoiceNumber;
  const payloadPassword = 'password: ' + password;
  const link = 'https://bit.ly/2NsLJbY';
  const phone = '91'+phoneNumber;
  const data = {
    "flow_id": "630f81947cb6eb56a6261474",
    "mobiles": phone,
    "inputCustomerInvoiceNumber": inputCustomerInvoiceNumber,
    "password": payloadPassword,
    "link": link
  }
  const dat_string = JSON.stringify(data);
  const xhr = new XMLHttpRequest();
  const url = "https://us-central1-scratchcardsreverseproxy.cloudfunctions.net/app/messageApi";
  if('withCredentials' in xhr) {
  xhr.open("POST", url , true);
  } else if ( typeof XDomainRequest != "undefined") {
    xhr = new XDomainRequest();
    xhr.open("POST", "https://us-central1-scratchcardsreverseproxy.cloudfunctions.net/app/messageApi");
  } else {
    xhr = null;
  }
  
  xhr.onerror = function() {
    alert('Woops, there was an error making a request');
  }
  xhr.setRequestHeader("content-type", "application/JSON");
  xhr.send(dat_string);

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === this.DONE) {
      const respo = JSON.parse(this.responseText);
      if(respo.type === "success"){
          alert('Message Successfully sent with id ' +  respo.message);
      } else if (respo.type === "error"){
          alert('Error sending message ' +  respo.message);
      }
    }
  });
}