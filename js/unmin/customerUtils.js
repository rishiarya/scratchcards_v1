var currCustScratchCards;
var currentCustomerRef;
var currentCustomerUsername;

// document.getElementById("HaHo").src = happyhome;
// document.getElementById("saddress").src = shopadd;
// document.getElementById("bdraw").src = bumperdraw;
// document.getElementById("Blogo").src = brandlogo;

(function () {
  if (Cookies.get('customerId') == null) {
    //User not present
    document.getElementById("cus_logged_div").style.display = "none";
    document.getElementById("cus_login_div").style.display = "block";
  } else {
    //User Logged in.
    document.getElementById("cus_logged_div").style.display = "block";
    document.getElementById("cus_login_div").style.display = "none";
    fetchCustomerDetails(Cookies.get('customerId'));
  }
})()

function customerLogin() {
  const inputCustomerId = document.getElementById("invoice_field").value
  const inputCustomerPassword = document.getElementById("customer_password_field").value;

  if (inputCustomerId == "" && inputCustomerPassword == "") {
    //Empty field show error
    window.alert("Empty username or password");
  } else {
    //Get all data
    db.collection("users").get().then(function (querySnapshot) {
      var docs = querySnapshot.docs;
      var isUserPresent = false;
      for (let i = 0; i < docs.length; i++) {
        if (inputCustomerId == docs[i].id && docs[i].data().password == inputCustomerPassword) {
          isUserPresent = true;
          break;
        }
      }

      if (isUserPresent) {
        Cookies.set('customerId', inputCustomerId, { expires: 7 });
        //Valid username and password
        //show age and scratch cards
        document.getElementById("cus_logged_div").style.display = "block";
        document.getElementById("cus_login_div").style.display = "none";
        fetchCustomerDetails(Cookies.get('customerId'));
      }
      else {
        window.alert("Invalid username and password");
      }
    });
  }

}
function fetchCustomerDetails(invoiceNum) {
  currentCustomerRef = db.collection("users").doc(invoiceNum);
  currentCustomerRef.get().then(function (doc) {
    if (doc.exists) {
      currCustScratchCards = doc.data();
      if (currCustScratchCards.scratchCards != null)
        prepareCardsGrid(currCustScratchCards);
      displayGiftCard(doc.data());
      // displayBOcard(doc.data());
    }
  }).catch(function (error) {
    console.log("Error: ", error);
  });

}
function prepareCardsGrid(scratchCards) {
  const totalValueOfScratchCard = scratchCards.scratchAmount;
  var noOfCards = scratchCards.scratchCards.noOfCards;
  var values = scratchCards.scratchCards.values;
  var processedCards = [];
  var queryValue = "";
  const queryStart = '<div class="box">';
  const queryEnd = '</div>';

  if (noOfCards <= 0) {
    console.log("Zero Cards retrieved");
  }
  else {
    for (let i = 0; i < noOfCards; i++) {
      var starIndex = values[i].startIndex;
      var endIndex = values[i].endIndex;

      if (starIndex == endIndex) {
        //Only 1 element
        var processedCard = {
          isOpened: false,
          values: []
        };
        queryValue = scratchCardValues[starIndex];
        processedCard.values.push(queryStart + queryValue + queryEnd);
        processedCard.isOpened = values[i].isOpened;
        processedCards.push(processedCard);
      }
      else if (endIndex < starIndex) {
        //Is out of bound, first process x to 99 followed by 0 to x
        var processedCard = {
          isOpened: false,
          values: []
        };
        //Process x to 99
        for (let j = starIndex; j <= 99; j++) {
          queryValue = scratchCardValues[j];
          processedCard.values.push(queryStart + queryValue + queryEnd);
        }

        //Process 0 to x
        for (let j = 0; j <= endIndex; j++) {
          queryValue = scratchCardValues[j];
          processedCard.values.push(queryStart + queryValue + queryEnd);
        }
        processedCard.isOpened = values[i].isOpened;
        processedCards.push(processedCard);

      } else {
        var processedCard = {
          isOpened: false,
          values: []
        };
        //Range is under control, process normally
        for (let j = starIndex; j <= endIndex; j++) {
          queryValue = scratchCardValues[j];
          processedCard.values.push(queryStart + queryValue + queryEnd);
        }
        processedCard.isOpened = values[i].isOpened;
        processedCards.push(processedCard);
      }


    }
  }
  if(getCountofisOpened(values, noOfCards)) {
    var processedCardLast = {
      isOpened: true,
      values: queryStart + "Total amount of \nRs. " + totalValueOfScratchCard + " has been Redeemed" + queryEnd 
    };  
  } else {
  //Create a last scratch card to display total value
  var processedCardLast = {
    isOpened: false,
    values: queryStart + "Congratulation!\n\nYou have won a total amount of \nRs. " + totalValueOfScratchCard + queryEnd
  };
}
  processedCards.push(processedCardLast);

  displayScratchCards(processedCards);
  console.log("processed grids", processedCards);
}

function getCountofisOpened( values, totalCards) {
  let count = 0;
    for( var i =0; i < values.length; i++ ) {
      if(values[i].isOpened == true){
        count++;
      }
    }
    return count === totalCards;
}

function displayScratchCards(cards) {

  $(document).ready(function () {
    var promoId;

    for (let i = 0; i < cards.length; i++) {
      promoId = 'promo' + i;
      if (i != cards.length - 1) {
        //Make sure it's not the last card
        const query = $('#scratch_card_container')
          .append($('<div id=' + '"' + promoId + '"' + 'class="scratchpad">')
            .append($('<div class="wrapper">')
              .append(cards[i].values)));
      } else {
        //Last card for displaying the amount
        const query = $('#scratch_card_container')
          .append($('<div id=' + '"' + promoId + '"' + 'class="scratchpad">')
            .append(cards[i].values));
      }
// if isOPened true then clear FG and only BG else add BG
      if(cards[i].isOpened) {
        // $('#' + promoId).wScratchPad({
        //   size: 70,
        //   // bg: selectBG
        // })
      } else {
      $('#' + promoId).wScratchPad({
        // the size of the eraser
        size: 70,
        // the randomized scratch image   
        bg: selectBG,
        // give real-time updates
        realtime: true,
        // The overlay image
        fg: scratchCardBG,
        // The cursor (coin) image
        // 'cursor': 'url("https://jennamolby.com/scratch-and-win/images/coin1.png") 5 5, default',

        scratchMove: function (e, percent) {
          // Show the plain-text promo code and call-to-action when the scratch area is 50% scratched
          if (percent > 50 && percent < 51) {
            if(!cards[i].isOpened) {
              updateScratchCardStatus(i);
            }
          }
        }
      });

      }
    }
  });
}

function updateScratchCardStatus(currentScratchCardIndex) {
  let tempData = {};
  const userRef = db.collection('users').doc(Cookies.get('customerId'));
  db.runTransaction(transaction => {
    // This code may get re-run multiple times if there are conflicts.
    return transaction.get(currentCustomerRef).then(doc => {
      tempData = doc.data();
      tempData.scratchCards.values[currentScratchCardIndex].isOpened = true;
      transaction.update(userRef, { scratchCards : tempData.scratchCards});
    });
  })
  .then(function () {
    console.log("Transaction successfully committed!");
  }).catch(function (error) {
    console.log("Transaction failed: ", error);
  });

}


function displayGiftCard(cards) {
  if (cards.giftCard == null || cards.giftCard == "")
    return;
// Dummy weight prompt 

  // if (Cookies.get('weight') == null || Cookies.get('weight') != 'false') {
  //   prompt("Please enter your weight.", "0");
  //   Cookies.set('weight', 'false', { expires: 1 });
  // }

  var currentBG;
  if (cards.giftCard == "a") {
    currentBG = giftABGUrl;
  } else if (cards.giftCard == "d") {
    currentBG = giftDUrl;
  }
  else if (cards.giftCard == "e") {
    currentBG = giftEUrl;
  }
  else if (cards.giftCard == "b") {
    currentBG = giftBUrl19;
  } else if (cards.giftCard == "c") {
    currentBG = giftCUrl19;
  }
  // }  else if (cards.giftCard == "h") {
  //   currentBG = giftH;
  // } else if (cards.giftCard == "g") {
  //   currentBG = giftG;
  // } else {
  //   currentBG = giftF;
  // }

  $(document).ready(function () {
    var promoId = "giftPromo";
    const query = $('#gift_card_container')
      .append($('<div id=' + '"' + promoId + '"' + 'class="gift-scratchpad">')
        .append($('<div class="gift-wrapper"></div>')
        ));
    if(!cards.giftIsopened ) {
    $('#' + promoId).wScratchPad({
      // the size of the eraser
      size: 70,
      // the randomized scratch image   
      bg: currentBG,
      // give real-time updates
      realtime: true,
      // The overlay image
      fg: giftBG,
      // The cursor (coin) image

      scratchMove: function (e, percent) {
        // Show the plain-text promo code and call-to-action when the scratch area is 50% scratched
        if (percent > 50 && percent < 51) {
          if(!cards.giftIsopened) {
            const userRef = db.collection('users').doc(Cookies.get('customerId'));
            userRef.update({ giftIsopened: true});
          }
        }
      }
    });
    } else {
      $('#' + promoId).wScratchPad({
        // the size of the eraser
        size: 70,
        // the randomized scratch image   
        bg: currentBG,
        // give real-time updates
        realtime: true,
        // The overlay image
        fg: '#ffffff00',
      });
    }
  });
}



// to enable BOCard un comment displayBOCard function and line 65, and first few dom lines of this file. and uncomment html elements from 95-144

// function displayBOcard(cards){
//   console.log(cards.name);
//   console.log(cards.invoiceNumber);
//   var inv = document.getElementById("invnum");
//   inv.innerHTML += cards.invoiceNumber;
//   var name = document.getElementById("name");
//   name.innerHTML += cards.name;
//   var pnum = document.getElementById("pn");
//   pnum.innerHTML += cards.phone;
// }
function savePDF(){
  var quality = 10;
  const filename  = 'Juturu\'s_Bumper_Offer.pdf';
 var imgData;
            html2canvas($("#bumperoffercard"), {
              scale: quality,
                useCORS: true,
                onrendered: function (canvas) {
                    imgData = canvas.toDataURL(
                       'image/png');
                    var doc = new jsPDF('p', 'mm', 'a4');
                    doc.addImage(imgData, 'PNG', 0, 0, 210, 300);
                    doc.save(filename);
                }
            });
            }


//  function savePDF(){
 
//  var HTML_Width = $("#bumperoffercard").width();
//  var HTML_Height = $("#bumperoffercard").height();
//  var top_left_margin = 15;
//  var PDF_Width = HTML_Width+(top_left_margin*2);
//  var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
//  var canvas_image_width = HTML_Width;
//  var canvas_image_height = HTML_Height;
 
//  var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
 
 
//  html2canvas($("#bumperoffercard")[0],{allowTaint:true}).then(function(canvas) {
//  canvas.getContext('2d');
 
//  console.log(canvas.height+"  "+canvas.width);
 
 
//  var imgData = canvas.toDataURL("image/jpeg", 1.0);
//  var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
//      pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
 
 
//  for (var i = 1; i <= totalPDFPages; i++) { 
//  pdf.addPage(PDF_Width, PDF_Height);
//  pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
//  }
 
//      pdf.save("Juturu\'s_Bumper_Offer.pdf");
//         });
//  };
function customerLogout() {
  document.getElementById("invoice_field").value = '';
  document.getElementById("customer_password_field").value = '';
  Cookies.remove('customerId');
  document.getElementById("cus_logged_div").style.display = "none";
  document.getElementById("cus_login_div").style.display = "block";
  document.location.reload(true);
}