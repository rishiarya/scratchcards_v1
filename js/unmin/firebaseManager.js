(function () {
  showSpinner(true);
  firebaseApp.auth().onAuthStateChanged(function (user) {
    if (user) {
      // User is signed in.
      showLoggedInUI();
      // if (firebase.auth().currentUser != null) {
      //   document.getElementById("user_para").innerHTML = "Welcome User : " + user.email;
      // }

      if (user.email.includes('juturu') || user.email != null || user.email != "" || user.email != 'undefined') {
        displayCustomerList(db.collection("managers").doc(getCurrentManagerIfAvailable(user.email)));
      }

    } else {
      // No user is signed in.
      if (Cookies.get('adminId') == null)
        showLogInUI();
    }
    showSpinner(false);
  });

  function showLoggedInUI() {
    document.getElementById("login_div").style.display = "none";
    document.getElementById("user_logged_in_wrapper").style.display = "block";
    document.getElementById("navbar_id").style.display = "block";
    document.getElementById("admin_logged_in").style.display = "none";
    document.getElementById("current_user_id_nav").innerHTML = Cookies.get('managerref') + "@juturu.com";
  }

  function showLogInUI() {
    document.getElementById("login_div").style.display = "block";
    document.getElementById("user_logged_in_wrapper").style.display = "none";
    document.getElementById("navbar_id").style.display = "none";
    document.getElementById("admin_logged_in").style.display = "none";

  }

  function getCurrentManagerIfAvailable(email) {
    var arr = email.split("@");
    var currentManager = arr[0];
    if (email != null) {
      if (email.includes("manager1")) {
        currentManager = "manager1";
        return currentManager;
      } else if (email.includes("manager2")) {
        currentManager = "manager2";
        return currentManager;
      } else if (email.includes("manager3")) {
        currentManager = "manager3";
        return currentManager;
      } else if (email.includes("manager4")) {
        return currentManager = "manager4";
        return currentManager;
      } else {
        return currentManager;
      }
    }
  }
})();

